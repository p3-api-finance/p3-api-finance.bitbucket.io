/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.6458219552157292, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9942528735632183, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [0.9607843137254902, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [0.9664804469273743, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.7549019607843137, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [0.008571428571428572, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [1.0, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [0.8491620111731844, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [0.865296803652968, 500, 1500, "bankAccountsByFkChild"], "isController": false}, {"data": [0.9933035714285714, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.503584229390681, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.9174107142857143, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [0.9666666666666667, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [0.9607843137254902, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.7549019607843137, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [0.7767857142857143, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.002793296089385475, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [0.0, 500, 1500, "getChildStatementOfAccount"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [0.9821428571428571, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [0.9933035714285714, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.3795620437956204, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.6125, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}, {"data": [0.7352941176470589, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3662, 0, 0.0, 1645.870562534134, 8, 28558, 438.5, 4798.000000000003, 6285.049999999999, 24327.289999999997, 11.98082805777756, 249.06082199497803, 21.121817924825375], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllFeeGroup", 174, 0, 0.0, 177.90804597701145, 9, 634, 165.0, 337.5, 380.75, 541.75, 0.5982547464981468, 0.569043088954292, 0.6449933985683145], "isController": false}, {"data": ["findAllChildActiveSubsidies", 51, 0, 0.0, 223.82352941176475, 20, 602, 177.0, 444.80000000000007, 520.8, 602.0, 0.18545252233611997, 0.2079842367628717, 0.3024469846692582], "isController": false}, {"data": ["findAllProgramBillingUpload", 179, 0, 0.0, 264.94972067039123, 23, 699, 248.0, 474.0, 541.0, 645.3999999999992, 0.5991491412753509, 0.8039985517075751, 0.8940428592468126], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 51, 0, 0.0, 519.843137254902, 157, 1076, 489.0, 765.8000000000004, 889.8, 1076.0, 0.18552943187977694, 0.26427925794593454, 0.33373556008061434], "isController": false}, {"data": ["findAllCurrentFeeTier", 175, 0, 0.0, 2243.6, 1228, 3531, 2219.0, 3106.8, 3294.4, 3486.1600000000008, 0.5870060344220338, 4.336991883383704, 1.4864322727112635], "isController": false}, {"data": ["findAllWithdrawalDraft", 105, 0, 0.0, 176.01904761904765, 9, 492, 168.0, 330.4, 374.4, 489.1199999999999, 0.35151267621665233, 0.17747270859766529, 1.140356553116913], "isController": false}, {"data": ["listBulkInvoiceRequest", 179, 0, 0.0, 396.5642458100561, 41, 1345, 379.0, 666.0, 732.0, 1045.7999999999956, 0.5979023314850692, 0.9207766362983499, 0.9061957211570579], "isController": false}, {"data": ["bankAccountsByFkChild", 219, 0, 0.0, 355.3424657534247, 37, 1155, 319.0, 657.0, 728.0, 1078.8000000000006, 0.7533461986976399, 1.0855554251246118, 1.73402049836947], "isController": false}, {"data": ["findAllCustomSubsidies", 224, 0, 0.0, 165.34375000000003, 31, 1208, 126.5, 335.0, 390.5, 665.0, 0.7480330737480465, 0.897391187694522, 0.9357718432336402], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 279, 0, 0.0, 904.7992831541219, 384, 2165, 855.0, 1323.0, 1497.0, 1908.5999999999995, 0.9311390934910374, 82.47354889147725, 1.3421497089773156], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 224, 0, 0.0, 282.6830357142857, 17, 1131, 254.5, 579.0, 707.75, 896.25, 0.7485455159116048, 0.5387480910418484, 0.9824659896339812], "isController": false}, {"data": ["getTransferDrafts", 105, 0, 0.0, 224.16190476190468, 8, 1228, 205.0, 462.4, 520.5999999999999, 1191.6399999999985, 0.3515632847394749, 0.17578164236973742, 0.6028761015649589], "isController": false}, {"data": ["getAllChildDiscounts", 51, 0, 0.0, 252.05882352941182, 13, 1047, 226.0, 463.00000000000006, 737.9999999999997, 1047.0, 0.18594747530726907, 0.1136749214280766, 0.30652279132682636], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 51, 0, 0.0, 481.6470588235293, 79, 957, 481.0, 811.2000000000002, 895.9999999999999, 957.0, 0.18547005749571782, 0.14924543689108544, 0.2470519125235929], "isController": false}, {"data": ["invoicesByFkChild", 56, 0, 0.0, 480.48214285714295, 53, 1228, 478.5, 816.0000000000003, 895.25, 1228.0, 0.18671894797209884, 0.532512384717721, 0.5428342852665413], "isController": false}, {"data": ["findAllFeeDraft", 179, 0, 0.0, 3232.106145251397, 1478, 4540, 3301.0, 4066.0, 4234.0, 4524.0, 0.59423821316885, 0.33774085943776433, 0.9470671522378546], "isController": false}, {"data": ["getChildStatementOfAccount", 223, 0, 0.0, 5631.085201793727, 3374, 7950, 5612.0, 6671.799999999999, 7038.799999999999, 7824.559999999997, 0.7376094837395146, 1.099618005940568, 1.2029373416454978], "isController": false}, {"data": ["findAllConsolidatedRefund", 100, 0, 0.0, 4365.7, 2782, 5961, 4340.5, 4978.1, 5300.65, 5959.699999999999, 0.3487017832609196, 0.4817192545278926, 0.6027364808318629], "isController": false}, {"data": ["getAdvancePaymentReceipts", 224, 0, 0.0, 179.9910714285714, 14, 791, 160.0, 394.5, 477.75, 741.75, 0.749514822994044, 1.1272000267683864, 1.1960031452854178], "isController": false}, {"data": ["getRefundChildBalance", 224, 0, 0.0, 140.35267857142858, 8, 988, 113.5, 293.0, 368.5, 632.0, 0.7494195344231143, 0.7150223487611158, 1.1738954425924562], "isController": false}, {"data": ["findAllUploadedGiroFiles", 274, 0, 0.0, 1324.379562043796, 571, 2891, 1296.0, 1710.0, 1889.75, 2437.25, 0.9336113723405705, 158.60221311574907, 1.3365959686047622], "isController": false}, {"data": ["findAllInvoice", 160, 0, 0.0, 8867.912499999993, 32, 28558, 427.0, 26408.9, 27269.899999999998, 28302.409999999993, 0.5246383274530121, 0.9388893437348347, 1.6824834790161065], "isController": false}, {"data": ["findAllCreditDebitNotes", 104, 0, 0.0, 7285.875000000002, 4967, 10978, 7235.5, 8633.5, 9716.0, 10966.5, 0.34478528827696864, 0.6578944642069773, 0.8057335887175645], "isController": false}, {"data": ["bankAccountInfoByIDChild", 51, 0, 0.0, 553.1176470588234, 61, 1543, 506.0, 903.8000000000001, 1174.7999999999993, 1543.0, 0.1859176494176403, 0.2655727391411334, 0.42811896223319906], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3662, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
